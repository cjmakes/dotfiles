#!/bin/sh
# Apparently this bit of code figures out where the script lives.
# http://stackoverflow.com/a/246128/264895

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
    DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cd "$DIR"

link(){
	echo "Linking config dirs..."
	for file in `find $DIR/dotfiles/* -prune`; do
		# Link based on dir or file
		if [[ -d $file ]]; then
			dest="$HOME/.config/`basename $file`"
		else
			dest="$HOME/.`basename $file`"
		fi

		# Backup existing files
		if [[ -e "$dest" ]]; then
			echo "Backing up existing $dest"
			mv $dest $dest.back
		fi

		# Link it!
		ln -s $file $dest
	done
}
